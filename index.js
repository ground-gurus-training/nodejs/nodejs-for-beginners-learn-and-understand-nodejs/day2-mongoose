const express = require("express");
const exphbr = require("express-handlebars");
const mongoose = require("mongoose");

const bodyParser = require("body-parser");

const app = express();
const port = process.env.PORT || 3000;

app.engine(
  "handlebars",
  exphbr({
    runtimeOptions: {
      allowProtoPropertiesByDefault: true,
      allowProtoMethodsByDefault: true,
    },
  })
);
app.set("view engine", "handlebars");
app.set("views", "./views");

app.use(express.static("public"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.connect(
  "mongodb+srv://cluster0.hhfbvy5.mongodb.net/nodejsday2?retryWrites=true&w=majority",
  {
    user: "groundgurus",
    pass: "Passw0rd1",
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

console.log("db connection successful.");

const noteSchema = mongoose.Schema({
  note: String,
  dateCreated: Date,
});

const Note = mongoose.model("Note", noteSchema);

app.get("/", async (req, res) => {
  const notes = await Note.find({}, (err, docs) => {
    return JSON.stringify(docs);
  });

  res.render("home", {
    notes,
  });
});

app.post("/api/note", async (req, res) => {
  const newNote = new Note();
  newNote.note = req.body.note;
  newNote.dateCreate = new Date();
  await newNote.save(() => {
    res.redirect("/");
  });
});

app.listen(port, () => {
  console.log(`Mongoose example running at http://localhost:${port}`);
});
